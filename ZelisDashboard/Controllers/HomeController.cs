﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZelisDashboard.Models;

namespace ZelisDashboard.Controllers
{
    public class HomeController : Controller
    {
        private string key = "accb452e632e9f887bba9c852c6a5e7f8ff8";

        public ActionResult Index()
        {

            //string encPW = System.Security.Encryption.Encrypt("Windstream001", key);
            string decPW = System.Security.Encryption.Decrypt("iXaApCuESPA=", key);
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            DashboardModel model = new DashboardModel();

            using (Entities db = new Entities())
            {
                var statsList = db.realtime_stats.ToList();
                var dailyStatsList = db.daily_stats.ToList();


                @ViewBag.stats_SkillID1 = statsList[0].Skill_Id;
                @ViewBag.stats_calls_in_queue1 = statsList[0].calls_in_queue;
                @ViewBag.stats_avg_answer_speed1 = (double)Math.Round(((decimal)statsList[0].avg_answer_speed), 2);
                @ViewBag.stats_active_calls1 = statsList[0].active_calls;
                @ViewBag.stats_Last_Updated1 = statsList[0].Last_Updated;

                @ViewBag.stats_SkillID2 = statsList[1].Skill_Id;
                @ViewBag.stats_calls_in_queue2 = statsList[1].calls_in_queue;
                @ViewBag.stats_avg_answer_speed2 = (double)Math.Round(((decimal)statsList[1].avg_answer_speed), 2);
                @ViewBag.stats_active_calls2 = statsList[1].active_calls;
                @ViewBag.stats_Last_Updated2 = statsList[1].Last_Updated;

                @ViewBag.stats_SkillID3 = statsList[2].Skill_Id;
                @ViewBag.stats_calls_in_queue3 = statsList[2].calls_in_queue;
                @ViewBag.stats_avg_answer_speed3 = (double)Math.Round(((decimal)statsList[2].avg_answer_speed), 2);
                @ViewBag.stats_active_calls3 = statsList[2].active_calls;
                @ViewBag.stats_Last_Updated3 = statsList[2].Last_Updated;

                @ViewBag.stats_SkillID4 = statsList[3].Skill_Id;
                @ViewBag.stats_calls_in_queue4 = statsList[3].calls_in_queue;
                @ViewBag.stats_avg_answer_speed4 = (double)Math.Round(((decimal)statsList[3].avg_answer_speed), 2);
                @ViewBag.stats_active_calls4 = statsList[3].active_calls;
                @ViewBag.stats_Last_Updated4 = statsList[3].Last_Updated;

                @ViewBag.stats_calls_in_queue_Total = statsList.Sum(s => s.calls_in_queue.Value);
                @ViewBag.stats_avg_answer_speed_Total = (double)Math.Round(((decimal)statsList.Sum(s=>s.avg_answer_speed.Value)), 2);
                @ViewBag.stats_active_calls_Total = statsList.Sum(s => s.active_calls.Value);

                ///////////////////////////////////////////////////////////////////////////////////////////////////
                @ViewBag.dailyStats_SkillID1 = dailyStatsList[0].Skill_Id;
                @ViewBag.dailyStats_calls_offered1 = dailyStatsList[0].calls_offered;
                @ViewBag.dailyStats_calls_handled1 = dailyStatsList[0].calls_handled;
                @ViewBag.dailyStats_avg_answer_speed1 = (double)Math.Round(((decimal)dailyStatsList[0].avg_answer_speed), 2);
                @ViewBag.dailyStats_Last_Updated1 = dailyStatsList[0].Last_Updated;

                @ViewBag.dailyStats_SkillID2 = dailyStatsList[1].Skill_Id;
                @ViewBag.dailyStats_calls_offered2 = dailyStatsList[1].calls_offered;
                @ViewBag.dailyStats_calls_handled2 = dailyStatsList[1].calls_handled;
                @ViewBag.dailyStats_avg_answer_speed2 = (double)Math.Round(((decimal)dailyStatsList[1].avg_answer_speed), 2);
                @ViewBag.dailyStats_Last_Updated2 = dailyStatsList[1].Last_Updated;

                @ViewBag.dailyStats_SkillID3 = dailyStatsList[2].Skill_Id;
                @ViewBag.dailyStats_calls_offered3 = dailyStatsList[2].calls_offered;
                @ViewBag.dailyStats_calls_handled3 = dailyStatsList[2].calls_handled;
                @ViewBag.dailyStats_avg_answer_speed3 = (double)Math.Round(((decimal)dailyStatsList[2].avg_answer_speed), 2);
                @ViewBag.dailyStats_Last_Updated3 = dailyStatsList[2].Last_Updated;

                @ViewBag.dailyStats_SkillID4 = dailyStatsList[3].Skill_Id;
                @ViewBag.dailyStats_calls_offered4 = dailyStatsList[3].calls_offered;
                @ViewBag.dailyStats_calls_handled4 = dailyStatsList[3].calls_handled;
                @ViewBag.dailyStats_avg_answer_speed4 = (double)Math.Round(((decimal)dailyStatsList[3].avg_answer_speed), 2);
                @ViewBag.dailyStats_Last_Updated4 = dailyStatsList[3].Last_Updated;

                @ViewBag.dailyStats_calls_offered_Total = dailyStatsList.Where(w=>w.Skill_Id.ToString() != "TOTALS").Sum(s => s.calls_offered.Value);
                @ViewBag.dailyStats_calls_handled_Total = dailyStatsList.Where(w => w.Skill_Id.ToString() != "TOTALS").Sum(s => s.calls_handled.Value);
                @ViewBag.dailyStats_avg_answer_speed_Total = (double)Math.Round(((decimal)dailyStatsList[4].avg_answer_speed), 2);


            }

            @ViewBag.Message = "";
            return View();
        }

        private string GetSkillName(int skillID)
        {
            string skillName = string.Empty;
            switch(skillID)
            {
                case 1250:
                    skillName = skillID.ToString() + "-Zelis Internally Down";
                    break;
                case 1252:
                    skillName = skillID.ToString() + "-TwillFax Overflow";
                    break;
                case 1253:
                    skillName = skillID.ToString() + "-VPC Status";
                    break;
                case 1254:
                    skillName = skillID.ToString() + "-Account Info Overflow";
                    break;
            }
            return skillName;
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            @ViewBag.Message = "";
            if (model.UserName == null || model.UserName.Trim() == string.Empty || model.Password == null || model.Password.Trim() == string.Empty)
            {
                @ViewBag.Message = "Please provide a Username and Password";
            }
            else
            {
                using (Entities db = new Entities())
                {
                    string encPW = System.Security.Encryption.Encrypt(model.Password, key);
                    var user = db.Users.Where(w => w.UserName == model.UserName && w.Password == encPW).FirstOrDefault();
                    if (user == null)
                    {
                        @ViewBag.Message = "Invalid Credentials";
                    }
                    else
                    {
                        Session["UserID"] = user.UserID;
                        return RedirectToAction("Index");
                    }
                }
                

            }

            
            return View();
        }
    }
}