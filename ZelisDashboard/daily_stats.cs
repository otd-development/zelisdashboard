//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZelisDashboard
{
    using System;
    using System.Collections.Generic;
    
    public partial class daily_stats
    {
        public int Id { get; set; }
        public string Skill_Id { get; set; }
        public Nullable<int> calls_offered { get; set; }
        public Nullable<int> calls_handled { get; set; }
        public Nullable<double> avg_answer_speed { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
    }
}
