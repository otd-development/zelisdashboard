﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace System.Security
{
    public static class Encryption
    {

        public static string Encrypt(string value, string key)
        {
            string res = string.Empty;
            MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();
            byte[] keyArr = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            objMD5CryptoService.Clear();
            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();
            objTripleDESCryptoService.Key = keyArr;
            byte[] toEncryptedArray = UTF8Encoding.UTF8.GetBytes(value);
            objTripleDESCryptoService.Mode = CipherMode.ECB;
            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;
            var objCrytpoTransform = objTripleDESCryptoService.CreateEncryptor();
            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptedArray, 0, toEncryptedArray.Length);
            objTripleDESCryptoService.Clear();
            res = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            return res;
        }
        public static string Decrypt(string value, string key)
        {
            MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();
            byte[] keyArr = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            objMD5CryptoService.Clear();
            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();
            objTripleDESCryptoService.Key = keyArr;
            objTripleDESCryptoService.Mode = CipherMode.ECB;
            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;
            byte[] toEncryptArray = Convert.FromBase64String(value);
            var objCrytpoTransform = objTripleDESCryptoService.CreateDecryptor();
            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            objTripleDESCryptoService.Clear();
            string res = UTF8Encoding.UTF8.GetString(resultArray);
            return res;
        }
    }
}
