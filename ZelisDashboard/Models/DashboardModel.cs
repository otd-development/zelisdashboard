﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZelisDashboard.Models
{
    public class DashboardModel
    {
        public ZelisDashboard.realtime_stats stats { get; set; }
        public ZelisDashboard.daily_stats dailyStats { get; set; }
    }
}